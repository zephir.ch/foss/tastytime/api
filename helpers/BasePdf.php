<?php

namespace app\helpers;

use TCPDF;

/**
 * Abstract Class BasePdf
 *
 * Extended by all PDF helpers.
 *
 * @author Basil Suter <basil@nadar.io>
 * @since 1.3.0
 */
abstract class BasePdf extends TCPDF
{
    /**
     * Constructors ensures that composer loads the TCPDF class.
     */
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false)
    {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);

        class_exists('TCPDF', true); // trigger Composers autoloader to load the TCPDF class
    }

    /**
     * {@inheritDoc}
     */
    public function Header()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function Footer()
    {
    }
}
