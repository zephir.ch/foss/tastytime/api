<?php

namespace app\helpers;

use app\modules\teamwork\admin\Module;
use luya\admin\models\Config;
use TCPDF;

class TimesheetPdf extends BasePdf
{
    /**
     * Constructors ensures that composer loads the TCPDF class.
     */
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false)
    {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache);

        class_exists('TCPDF', true); // trigger Composers autoloader to load the TCPDF class
    }

    public static function create($array, $from, $to, $customerName, $costunitName)
    {
        // Create new PDF object
        $pdf = new self('L');

        // Define font to be used
        $font = 'helvetica';

        // Set PDF meta data
        $pdf->SetAuthor(Config::get('tastytime_pdf_author'));
        $pdf->SetTitle('Zeiterfassung ' . $customerName . ': ' . $costunitName);
        $pdf->setFontSubsetting(false);
        $pdf->setMargins(20, 25);
        $pdf->SetAutoPageBreak(true, 20);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);

        // Add first page to PDF
        $pdf->AddPage();

        // Write title (customer, project, time frame)
        $pdf->setCellPaddings(0);
        $pdf->setFont($font, '', 13);
        $pdf->Cell(0, 0, 'Zeiterfassung ' . $customerName, 0, 1);
        $pdf->Cell(0, 0, $costunitName . ' vom ' . date("d.m.Y", $from). " bis " . date("d.m.Y", $to), 0, 1);
        $pdf->Ln(6);

        // Define widths of table columns
        $columnWidths = [21, 32, 142, 20, 18, 24];

        // Set paddings for table cells
        $pdf->setCellPaddings(0, 1, 0, 1);

        // Write table title row
        $pdf->printPdfTableTitleRow($pdf, $font, $columnWidths);

        // Write table data rows
        foreach ($array['rows'] as $i => $row) {
            // Store cleaned-up job text in variable as it is used twice
            $jobText = strip_tags((string) $row['composedMessage']);

            // Evaluate height of job text cell to use it for all other cells of same row
            $cellHeight = $pdf->getStringHeight($columnWidths[2], $jobText);

            // Calculate and format time in hours
            $time = number_format($row['min_rounded'] / 60, 2, '.', '\'');

            // Write data row using a transaction to roll back if page break occurrs while writing
            do {
                // Flag whether to repeat the writing of current row
                $repeat = false;

                // Remember current page before writing
                $startPage = $pdf->getPage();

                // Start transaction
                $pdf->startTransaction();

                // Reset font and border styles for every row as title row after page break will overwrite styles
                $pdf->setFont($font, '', 9);
                $pdf->SetLineStyle(['width' => 0.15]);

                // Write cells of row
                $pdf->Cell($columnWidths[0], $cellHeight, $row['date'], 'B', 0, 'L', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[1], $cellHeight, $row['user'], 'B', 0, 'L', false, '', 0, false, 'T', 'T');
                $pdf->MultiCell($columnWidths[2], $cellHeight, $jobText, 'B', 'L', 0, 0, '', '', true, 0, false, true, 0, 'T');
                $pdf->Cell($columnWidths[3], $cellHeight, $time, 'B', 0, 'R', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[4], $cellHeight, Module::getInstance()->hourRate, 'B', 0, 'R', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[5], $cellHeight, $row['cost'], 'B', 1, 'R', false, '', 0, false, 'T', 'T');

                // Evaluate current page after writing
                $endPage = $pdf->getPage();

                // If a page break occurred during writing…
                if ($endPage != $startPage) {
                    // Roll back transaction
                    $pdf = $pdf->rollbackTransaction();
                    // Manually add a new page to PDF
                    $pdf->AddPage();
                    // Print table title row on new page
                    self::printPdfTableTitleRow($pdf, $font, $columnWidths);
                    // Set flag to rewrite current table data row
                    $repeat = true;
                }
                // …otherwise…
                else {
                    // Commit transaction (releasing memory)
                    $pdf->commitTransaction();
                }
            } while ($repeat);
        }

        $flatFee = Config::get('tastytime_flat_fee', 0);

        // Calculate totals
        $totalTime = number_format($array['total_min'] / 60, 2, '.', '\'');
        $totalCost1 = $array['total_cost_raw'];

        if ($flatFee) {
            $additionalCost = $array['total_cost_raw'] / $flatFee; // 10% flat fee for project management
            $totalCost2 = $totalCost1 + $additionalCost;
        } else {
            $totalCost2 = $totalCost1;
        }

        // Write totals rows using a transaction to roll back if page break occurrs while writing
        do {
            // Flag whether to repeat the writing of current row
            $repeat = false;

            // Remember current page before writing
            $startPage = $pdf->getPage();

            // Start transaction
            $pdf->startTransaction();


            if ($flatFee) {
                // Write first total row
                $pdf->setFont($font, 'B', 9);
                $pdf->SetLineStyle(['width' => 0.4]);
                $pdf->Cell($columnWidths[0], 0, '', 'T', 0, 'L', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[1], 0, '', 'T', 0, 'L', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[2], 0, 'Zwischentotal', 'T', 0, 'L', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[3], 0, $totalTime, 'T', 0, 'R', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[4], 0, Module::getInstance()->hourRate, 'T', 0, 'R', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[5], 0, number_format($totalCost1, 2, '.', '\''), 'T', 1, 'R', false, '', 0, false, 'T', 'T');

                // Write row with flat fee for project management
                $pdf->setFont($font, '', 9);
                $pdf->SetLineStyle(['width' => 0.15]);
                $pdf->Cell($columnWidths[0], 0, '', 'TB', 0, 'L', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[1], 0, '', 'TB', 0, 'L', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[2], 0, '+10% pauschal für Projektmanagement und weitere administrative Aufwände', 'TB', 0, 'L', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[3], 0, '', 'TB', 0, 'R', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[4], 0, '', 'TB', 0, 'R', false, '', 0, false, 'T', 'T');
                $pdf->Cell($columnWidths[5], 0, number_format($additionalCost, 2, '.', '\''), 'TB', 1, 'R', false, '', 0, false, 'T', 'T');
            }

            // Write second total row
            $pdf->setFont($font, 'B', 9);
            $pdf->SetLineStyle(['width' => 0.4]);
            $pdf->Cell($columnWidths[0], 0, '', 'B', 0, 'L', false, '', 0, false, 'T', 'T');
            $pdf->Cell($columnWidths[1], 0, '', 'B', 0, 'L', false, '', 0, false, 'T', 'T');
            $pdf->Cell($columnWidths[2], 0, 'Gesamttotal', 'B', 0, 'L', false, '', 0, false, 'T', 'T');
            $pdf->Cell($columnWidths[3], 0, '', 'B', 0, 'R', false, '', 0, false, 'T', 'T');
            $pdf->Cell($columnWidths[4], 0, '', 'B', 0, 'R', false, '', 0, false, 'T', 'T');
            $pdf->Cell($columnWidths[5], 0, number_format($totalCost2, 2, '.', '\''), 'B', 1, 'R', false, '', 0, false, 'T', 'T');

            // Evaluate current page after writing
            $endPage = $pdf->getPage();

            // Remember current page before writing
            if ($endPage != $startPage) {
                // Roll back transaction
                $pdf = $pdf->rollbackTransaction();
                // Manually add a new page to PDF
                $pdf->AddPage();
                // Set flag to rewrite all totals rows
                $repeat = true;
            }
            // …otherwise…
            else {
                // Commit transaction (releasing memory)
                $pdf->commitTransaction();
            }
        } while ($repeat);

        // Manually place page numbers, going through all pages of the PDF
        $pdf->setFont($font, '', 9);
        $countPages = $pdf->getNumPages();
        for ($p = 1; $p <= $countPages; $p++) {
            $pdf->setPage($p, true);
            $pdf->SetAutoPageBreak(false); // necessary to place page numbers in margin area
            $pdf->setY(197);
            $pdf->Cell(0, 0, sprintf('Seite %d von %d', $p, $countPages), 0, 1, 'R', false, '', 0, false, 'T', 'T');
        }

        return $pdf;
    }

    /**
     * {@inheritDoc}
     */
    public function Header()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function Footer()
    {
    }

    public static function printPdfTableTitleRow($pdf, $font, $columnWidths)
    {
        $pdf->setFont($font, 'B', 9);
        $pdf->SetLineStyle(['width' => 0.4]);
        $pdf->Cell($columnWidths[0], 0, 'Datum', 'B', 0, 'L', false, '', 0, false, 'T', 'T');
        $pdf->Cell($columnWidths[1], 0, 'Mitarbeiter', 'B', 0, 'L', false, '', 0, false, 'T', 'T');
        $pdf->Cell($columnWidths[2], 0, 'Arbeit', 'B', 0, 'L', false, '', 0, false, 'T', 'T');
        $pdf->Cell($columnWidths[3], 0, 'Stunden', 'B', 0, 'R', false, '', 0, false, 'T', 'T');
        $pdf->Cell($columnWidths[4], 0, 'Ansatz', 'B', 0, 'R', false, '', 0, false, 'T', 'T');
        $pdf->Cell($columnWidths[5], 0, 'Kosten', 'B', 1, 'R', false, '', 0, false, 'T', 'T');
    }
}
