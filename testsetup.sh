
#!/bin/sh
set -e
./vendor/bin/luya migrate --interactive=0
./vendor/bin/luya import
./vendor/bin/luya admin/setup --email=admin@admin.com --password=admin --firstname=John --lastname=Doe --interactive=0
## add option to create api user