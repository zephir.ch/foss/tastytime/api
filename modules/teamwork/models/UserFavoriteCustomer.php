<?php

namespace app\modules\teamwork\models;

use luya\admin\ngrest\base\NgRestModel;
use luya\behaviors\TimestampBehavior;
use Yii;

/**
 * User Favorite Customer.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $customer_id
 * @property integer $sortindex
 * @property integer $created_at
 * @property integer $updated_at
 */
class UserFavoriteCustomer extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tw_user_favorite_customer}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'user-favorite-customer';
    }

    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::class],
        ];
    }

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_BEFORE_INSERT, function () {
            $this->sortindex = 1;
        });

        $this->on(self::EVENT_BEFORE_VALIDATE, function () {
            if (Yii::$app->jwt->identity) {
                $this->user_id = Yii::$app->jwt->identity->id;
            }
        });
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'sortindex' => Yii::t('app', 'Sortindex'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'customer_id'], 'required'],
            [['user_id', 'customer_id', 'sortindex', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'user_id' => 'number',
            'customer_id' => 'number',
            'sortindex' => 'number',
            'created_at' => 'number',
            'updated_at' => 'number',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['user_id', 'customer_id', 'sortindex', 'created_at', 'updated_at']],
            [['create', 'update'], ['user_id', 'customer_id', 'sortindex', 'created_at', 'updated_at']],
            ['delete', true],
        ];
    }
}
