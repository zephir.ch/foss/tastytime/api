<?php

namespace app\modules\teamwork\models;

use app\modules\teamwork\admin\Module;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\ngrest\plugins\SelectModel;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * Time.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $id
 * @property string $user_id
 * @property integer $issue_id
 * @property integer $costunit_id
 * @property text $message
 * @property integer $duration
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_active
 * @property integer $is_interruption
 * @property integer $is_charged
 * @property integer $is_not_chargeable
 * @property integer $exported_at
 * @property text $comment
 * @property Costunit $costunit
 * @property Issue $issue
 */
class Time extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tw_time}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'time';
    }

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_BEFORE_VALIDATE, function () {
            if (Yii::$app->jwt->identity) {
                $this->user_id = Yii::$app->jwt->identity->id;
            }
        });

        $this->on(self::EVENT_BEFORE_INSERT, function () {
            if ($this->costunit) {
                $this->is_not_chargeable = (int) !$this->costunit->is_billable;
            } elseif ($this->issue && $this->issue->gitlabProject && $this->issue->gitlabProject->costunit) {
                $this->is_not_chargeable = (int) !$this->issue->gitlabProject->costunit->is_billable;
            }
        });

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'deactivatedOthers']);
        $this->on(self::EVENT_AFTER_UPDATE, [$this, 'deactivatedOthers']);
    }

    public function deactivatedOthers()
    {
        if ($this->is_active && Yii::$app->jwt->identity) {
            self::updateAll(['is_active' => 0], [
                'and',
                ['=', 'user_id', Yii::$app->jwt->identity->id],
                ['!=', 'id', $this->id],
            ]);
        }
    }

    public function behaviors()
    {
        return [
            [
                // update
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => false,
            ],
            [
                // create
                'class' => TimestampBehavior::class,
                'preserveNonEmptyValues' => true,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'issue_id' => Yii::t('app', 'Issue ID'),
            'costunit_id' => Yii::t('app', 'Costunit ID'),
            'message' => Yii::t('app', 'Message'),
            'duration' => Yii::t('app', 'Duration (Sekunden)'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['issue_id', 'user_id', 'costunit_id', 'duration', 'created_at', 'updated_at', 'is_active', 'is_interruption', 'is_charged', 'is_not_chargeable', 'exported_at'], 'integer'],
            [['message', 'comment'], 'string'],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        $fields[] = 'costunit';
        $fields[] = 'issue';

        return $fields;
    }

    public function ngRestFilters()
    {
        $filters = [
            'Nicht Verrechnet' => self::ngRestFind()->where(['is_charged' => false]),
            'Aktiv' => self::ngRestFind()->where(['is_active' => true]),
            'Nicht verrechenbar' => self::ngRestFind()->where(['is_not_chargeable' => true]),
        ];

        foreach (User::find()->select(['id', 'email'])->asArray()->all() as $user) {
            $filters[$user['email']] = self::ngRestFind()->andWhere(['user_id' => $user['id']]);
        }

        return $filters;
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'user_id' => ['class' => SelectModel::class, 'modelClass' => User::class, 'labelField' => ['firstname', 'lastname']],
            'issue_id' => ['class' => SelectModel::class, 'modelClass' => Issue::class, 'labelField' => ['title']],
            'costunit_id' => ['class' => SelectModel::class, 'modelClass' => Costunit::class, 'labelField' => function ($model) {
                if ($model->customer) {
                    return $model->customer->name . ' - ' . $model->name;
                }

                return 'unknown - ' . $model->name;
            }],
            'message' => 'raw',
            'comment' => 'raw',
            'duration' => 'number',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
            'exported_at' => 'datetime',
            'is_not_chargeable' => 'toggleStatus',
            'is_active' => 'toggleStatus',
            'is_charged' => 'toggleStatus',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['user_id', 'message', 'duration', 'created_at', 'updated_at', 'is_not_chargeable']],
            [['create'], ['user_id', 'issue_id', 'costunit_id', 'message', 'comment', 'duration', 'is_not_chargeable', 'created_at']],
            [['update'], ['user_id', 'issue_id', 'costunit_id', 'message', 'comment', 'duration', 'is_charged', 'is_not_chargeable', 'created_at', 'exported_at']],
            ['delete', true],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getIssue()
    {
        return $this->hasOne(Issue::class, ['id' => 'issue_id']);
    }

    public function getCostunit()
    {
        return $this->hasOne(Costunit::class, ['id' => 'costunit_id']);
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::class, ['id' => 'customer_id'])->via('costunit');
    }

    public function price($minutes)
    {
        return ceil($minutes * Module::getInstance()->hourRate / 60);
    }

    public function formatPrice($price)
    {
        //return Yii::$app->formatter->asCurrency($price, 'CHF');
        return number_format($price, 2, '.', '\'');
    }

    public function roundedMinutes()
    {
        $minutes = $this->duration / 60;

        $rounded = round($minutes / 15) * 15;

        if ($rounded <= 14) {
            $rounded = 15;
        }

        return $rounded;
    }

    public function notRoundedMinutes()
    {
        return ceil($this->duration / 60);
    }
}
