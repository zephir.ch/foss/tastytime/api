<?php

namespace app\modules\teamwork\models;

use luya\admin\ngrest\base\NgRestModel;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * Customer.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 * @property tinyint $is_active
 * @property integer $created_at
 * @property integer $updated_at
 * @property Costunit[] $costunits
 */
class Customer extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tw_customer}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'customers';
    }

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_DELETE, function () {
            UserFavoriteCustomer::deleteAll(['customer_id' => $this->id]);
            foreach ($this->costunits as $costunit) {
                $costunit->delete();
            }
        });
    }

    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::class],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'color' => 'Farbe',
            'is_active' => Yii::t('app', 'Is Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_active', 'created_at', 'updated_at'], 'integer'],
            [['name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'name' => 'text',
            'is_active' => ['toggleStatus', 'initValue' => 1],
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
            'color' => 'color',
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['costunits'] = 'costunits';
        $fields['userFavorite'] = 'userFavorite';
        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['name', 'is_active']],
            [['create', 'update'], ['name', 'color', 'is_active']],
            ['delete', true],
        ];
    }

    public function getCostunits()
    {
        return $this->hasMany(Costunit::class, ['customer_id' => 'id']);
    }

    public function getUserFavorite()
    {
        return $this->hasOne(UserFavoriteCustomer::class, ['customer_id' => 'id'])->onCondition(['user_id' => Yii::$app->jwt->identity ? Yii::$app->jwt->identity->id : Yii::$app->adminuser->id]);
    }
}
