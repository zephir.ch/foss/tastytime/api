<?php

namespace app\modules\teamwork\models;

use app\modules\teamwork\admin\aws\UserTimeActiveWindow;
use app\modules\teamwork\admin\Module;
use Curl\Curl;
use Lcobucci\JWT\Token\Plain;
use luya\admin\aws\ChangePasswordActiveWindow;
use luya\admin\aws\ChangePasswordInterface;
use luya\admin\aws\DetailViewActiveWindow;
use luya\admin\base\JwtIdentityInterface;
use luya\admin\models\Logger;
use luya\admin\ngrest\base\NgRestModel;
use Yii;
use yii\base\InvalidCallException;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;

/**
 * User.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $jwt
 * @property string $firstname
 * @property string $lastname
 * @property string $gitlab_token
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $gitlab_user_id
 */
class User extends NgRestModel implements JwtIdentityInterface, ChangePasswordInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tw_user}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'users';
    }

    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::class],
        ];
    }

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_BEFORE_INSERT, function () {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function changePassword($newPassword)
    {
        $this->password = Yii::$app->security->generatePasswordHash($newPassword);
        $this->save(true, ['password']);

        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'jwt' => Yii::t('app', 'Jwt'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'gitlab_token' => Yii::t('app', 'Gitlab Token'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'gitlab_user_id' => 'Gitlab User Id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'firstname', 'lastname'], 'required'],
            [['created_at', 'updated_at', 'gitlab_user_id'], 'integer'],
            [['jwt'], 'string'],
            [['email', 'password', 'firstname', 'lastname', 'gitlab_token'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['password'], 'unique'],
            [['jwt'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'email' => 'text',
            'password' => 'password',
            'jwt' => 'text',
            'firstname' => 'text',
            'lastname' => 'text',
            'gitlab_token' => 'text',
            'created_at' => 'number',
            'updated_at' => 'number',
            'gitlab_user_id' => 'number',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['email', 'firstname', 'lastname']],
            [['create'], ['email', 'password', 'firstname', 'lastname', 'gitlab_user_id', 'gitlab_token']],
            [['update'], ['email', 'firstname', 'lastname', 'gitlab_user_id', 'gitlab_token']],
            ['delete', false],
        ];
    }

    public function ngRestActiveWindows()
    {
        return [
            ['class' => UserTimeActiveWindow::class],
            ['class' => DetailViewActiveWindow::class],
            ['class' => ChangePasswordActiveWindow::class],

        ];
    }

    // GITLAB ISSUES

    public function fetchGitLabIssues()
    {
        // if not gitlab token, we skip this..
        if (empty($this->gitlab_token)) {
            return;
        }

        // session call will import issues from gitlab:
        $curl = new Curl();
        $curl->setHeader('Authorization', 'Bearer ' . $this->gitlab_token);
        $url = 'https://gitlab.com/api/v4/issues?scope=assigned_to_me&per_page=100&labels='.urlencode(Module::getInstance()->readyLabel).'&with_labels_details=true&state=opened';
        $curl->get($url); // &labels=read

        if (!$curl->isSuccess()) {
            throw new InvalidCallException($curl->error_message);
        }

        $json = Json::decode($curl->response);

        Logger::info("{$this->email}: Make GitLab API Request {$url} with result count " . (is_array($json) || $json instanceof \Countable ? count($json) : 0));

        $handledIds = [];
        foreach ($json as $item) {
            $model = Issue::createOrSelect($item['id'], $this->id);
            $model->title = $item['title'];

            if ($item['state'] == 'closed') {
                $model->is_closed = 1;
            } else {
                $model->is_closed = 0;
            }

            if ($item['milestone']) {
                $model->milestone = $item['milestone']['title'];
            }
            $model->labels = Json::encode($item['labels']);

            // only fetch project when is not closed or its a new record
            if (!$model->is_closed || $model->isNewRecord) {
                $pr = $this->fetchProject($item['_links']['project'], $this);
                $model->project = $pr['name'];
                $model->project_id = $pr['id'];
            }
            $model->gitlab_url = $item['web_url'];
            $save = $model->save();

            if (!$save) {
                // save
                // var_dump($model->getErrors());
            } else {
                $handledIds[] = $model->id;
            }
        }

        $rows = Issue::updateAll(['is_closed' => 1], [
            'and',
            ['=', 'user_id', $this->id],
            ['not in', 'id', $handledIds],
        ]);

        \Yii::debug('handled rows ' . $rows, __METHOD__);
    }

    private $_projects = [];

    private function fetchProject($url, User $user)
    {
        if (isset($this->_projects[$url])) {
            return $this->_projects[$url];
        }
        $curl = new Curl();
        $curl->setHeader('Authorization', 'Bearer ' . $user->gitlab_token);
        $curl->get($url);

        if (!$curl->isSuccess()) {
            throw new InvalidCallException($curl->error_message);
        }

        $json = Json::decode($curl->response);

        $project = GitlabProject::find()->where(['gitlab_id' => $json['id']])->one();

        if (!$project) {
            $project = new GitlabProject();
            $project->gitlab_id = $json['id'];
        }

        $project->title = $json['name_with_namespace'];
        $project->save();

        $name = $json['name_with_namespace'];
        $this->_projects[$url] = ['name' => $name, 'id' => $project->id];
        return $this->_projects[$url];
    }

    // JWT

    public function getId()
    {
        return $this->id;
    }

    public static function loginByJwtToken(Plain $token)
    {
        // $userId = $token->claims()->get('uid');
        return self::findOne(['id' => $token->claims()->get('uid')]);
    }
}
