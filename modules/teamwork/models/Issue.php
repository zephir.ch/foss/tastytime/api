<?php

namespace app\modules\teamwork\models;

use luya\admin\ngrest\base\NgRestModel;
use luya\helpers\Json;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * Issue.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $id
 * @property string $title
 * @property string $project
 * @property string $milestone
 * @property text $labels
 * @property integer $gitlab_id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $gitlab_url
 * @property integer $is_closed
 * @property integer $project_id
 */
class Issue extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tw_issue}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'issues';
    }

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_FIND, function () {
            $this->labels = Json::isJson($this->labels) ? Json::decode($this->labels) : [];
        });
    }

    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::class],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'project' => Yii::t('app', 'Project'),
            'milestone' => Yii::t('app', 'Milestone'),
            'labels' => Yii::t('app', 'Labels'),
            'gitlab_id' => Yii::t('app', 'Gitlab ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'user_id'], 'required'],
            [['labels'], 'string'],
            [['gitlab_id', 'user_id', 'created_at', 'updated_at', 'is_closed', 'project_id'], 'integer'],
            [['title', 'project', 'milestone', 'gitlab_url'], 'string', 'max' => 255],
            //[['gitlab_id'], 'unique', 'targetAttribute' => ['gitlab_id', 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'title' => 'raw',
            'project' => 'raw',
            'milestone' => 'raw',
            'labels' => 'raw',
            'gitlab_url' => 'raw',
            'gitlab_id' => 'number',
            'user_id' => 'number',
            'project_id' => 'number',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
            'is_closed' => 'toggleStatus',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['title', 'project', 'updated_at']],
            [['create', 'update'], ['title', 'project', 'milestone', 'labels', 'gitlab_id', 'gitlab_url', 'project_id', 'user_id', 'created_at', 'updated_at', 'is_closed']],
            ['delete', false],
        ];
    }

    public static function createOrSelect($gitlabId, $userId)
    {
        $model = self::find()->where(['gitlab_id' => $gitlabId, 'user_id' => $userId])->one();

        if ($model) {
            return $model;
        }

        $model = new self();
        $model->gitlab_id = $gitlabId;
        $model->user_id = $userId;

        return $model;
    }

    public function getGitlabProject()
    {
        return $this->hasOne(GitlabProject::class, ['id' => 'project_id']);
    }
}
