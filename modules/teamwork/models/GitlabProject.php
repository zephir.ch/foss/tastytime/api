<?php

namespace app\modules\teamwork\models;

use luya\admin\ngrest\base\NgRestModel;
use luya\admin\ngrest\plugins\SelectModel;
use Yii;

/**
 * Gitlab Project.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $id
 * @property integer $costunit_id
 * @property integer $gitlab_id
 * @property string $title
 */
class GitlabProject extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tw_gitlab_project}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'gitlab-projects';
    }

    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_DELETE, function () {
            Issue::deleteAll(['project_id' => $this->id]);
        });
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'costunit_id' => Yii::t('app', 'Costunit ID'),
            'gitlab_id' => Yii::t('app', 'Gitlab ID'),
            'title' => Yii::t('app', 'Title'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['costunit_id', 'gitlab_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'costunit_id' => ['class' => SelectModel::class, 'modelClass' => Costunit::class, 'labelField' => function ($model) {
                if (is_numeric($model->customer_id) && $model->customer) {
                    return $model->customer->name . " | "  . $model->name;
                } else {
                    return $model->customer_id . " | "  . $model->name;
                }
            }],
            'gitlab_id' => 'number',
            'title' => 'text',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['costunit_id', 'title']],
            [['create', 'update'], ['costunit_id', 'gitlab_id', 'title']],
            ['delete', true],
        ];
    }

    public function getCostunit()
    {
        return $this->hasOne(Costunit::class, ['id' => 'costunit_id']);
    }
}
