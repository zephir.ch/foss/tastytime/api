<?php

namespace app\modules\teamwork\models;

use app\modules\teamwork\admin\aws\ExportActiveWindow;
use app\modules\teamwork\admin\buttons\MarkAsUnchargeable;
use luya\admin\ngrest\base\NgRestModel;
use luya\admin\ngrest\plugins\SelectModel;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * Costunit.
 *
 * File has been created with `crud/create` command.
 *
 * @property integer $id
 * @property string $name
 * @property integer $customer_id
 * @property tinyint $is_active
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_message_required
 * @property integer $is_billable
 * @property integer $time_budget_hours
 */
class Costunit extends NgRestModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tw_costunit}}';
    }

    /**
     * @inheritdoc
     */
    public static function ngRestApiEndpoint()
    {
        return 'costunits';
    }



    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::class],
        ];
    }

    public function init()
    {
        parent::init();
        /*
        // currently delete of time items is not suppored due to security concerns
        $this->on(self::EVENT_AFTER_DELETE, function() {
            Time::deleteAll(['costunit_id' => $this->id]);
        });
        */
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'is_active' => Yii::t('app', 'Is Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'openTime' => 'Billable Time',
            'time_budget_hours' => 'Time Budget (in Hours)'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'customer_id'], 'required'],
            [['customer_id', 'is_active', 'created_at', 'updated_at', 'is_message_required', 'is_billable', 'time_budget_hours'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeHints()
    {
        return [
            'time_budget_hours' => 'Number of hours which are calculated for this project. This allows to display whether you are close to the time budget or not',
        ];
    }

    /**
     * @inheritdoc
     */
    public function ngRestAttributeTypes()
    {
        return [
            'name' => 'text',
            'customer_id' => [
                'class' => SelectModel::class,
                'modelClass' => Customer::class,
                'labelField' => 'name',
                'sortField' => 'tw_customer.name'
            ],
            'is_active' => ['toggleStatus', 'initValue' => 1],
            'created_at' => 'number',
            'updated_at' => 'number',
            'time_budget_hours' => 'number',
            'is_message_required' => ['toggleStatus', 'initValue' => 1],
            'is_billable' => ['toggleStatus', 'initValue' => 1, 'interactive' => 0],
        ];
    }

    public function getOpenTime()
    {
        return $this->getTotalTime()->count();
    }

    public function ngRestExtraAttributeTypes()
    {
        return [
            'openTime' => ['number', 'sortField' => false],
        ];
    }

    public function ngRestGroupByField()
    {
        return 'is_billable';
    }

    /**
     * @inheritdoc
     */
    public function ngRestScopes()
    {
        return [
            ['list', ['customer_id', 'name', 'openTime', 'is_active', 'is_message_required', 'is_billable']],
            [['create', 'update'], ['name', 'customer_id', 'is_active', 'is_message_required', 'is_billable', 'time_budget_hours']],
            ['delete', true],
        ];
    }

    public function ngRestActiveWindows()
    {
        return [
            ['class' => ExportActiveWindow::class],
        ];
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::class, ['id' => 'customer_id']);
    }

    public function getTimes()
    {
        return $this->hasMany(Time::class, ['costunit_id' => 'id']);
    }

    public function getProjectTimes()
    {
        return $this->hasMany(GitlabProject::class, ['costunit_id' => 'id']);
    }

    public function getIssues()
    {
        return $this->hasMany(Issue::class, ['project_id' => 'id'])->via('projectTimes');
    }

    public function ngRestFilters()
    {
        $q = '
        SELECT c1.id, count(c1.id) AS counter 
        FROM tw_costunit AS c1
        INNER JOIN tw_time AS t1
            ON c1.id = t1.costunit_id
            OR t1.id IN (SELECT t2.id FROM tw_time AS t2
                INNER JOIN tw_issue AS i
                    ON t2.issue_id = i.id
                INNER JOIN tw_gitlab_project AS p
                    ON i.project_id = p.id
                INNER JOIN tw_costunit AS c2
                    ON p.costunit_id = c2.id
                WHERE c2.id = c1.id)
        WHERE t1.is_charged = 0 AND t1.is_not_chargeable = 0
        GROUP BY c1.id';

        $subQuery = Yii::$app->db->createCommand($q)->queryColumn();

        return [
            'Offene' => self::ngRestFind()->andWhere(['in', 'tw_costunit.id', $subQuery])->joinWith(['customer']),
        ];
    }

    public function ngRestFullQuerySearch($query)
    {
        $query = parent::ngRestFullQuerySearch($query);
        $query->joinWith(['customer']);
        return $query;
    }

    public function getTotalTime()
    {
        return Time::find()->where([
            'or',
            ['in', 'issue_id', $this->getIssues()->select(['id'])],
            ['=', 'costunit_id', $this->id],
        ])->andWhere([
            'is_charged' => false,
            'is_not_chargeable' => false,
        ]);
    }

    public function getTotalTimeAll()
    {
        return Time::find()->where([
            'or',
            ['in', 'issue_id', $this->getIssues()->select(['id'])],
            ['=', 'costunit_id', $this->id],
        ]);
    }

    public function ngRestActiveButtons()
    {
        return [
            [
                'class' => MarkAsUnchargeable::class,
            ]
        ];
    }
}
