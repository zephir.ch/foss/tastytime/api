<?php

namespace app\modules\teamwork\admin\traits;

use Yii;

trait TimeDataGenerator
{
    public function generateArray(array $items)
    {
        $array = [];
        $totalMin = 0;
        $totalMinRaw = 0;
        $totalCost = 0;
        foreach ($items as $i) {
            $msg = null;
            if ($i->issue) {
                if (empty($i->message)) {
                    $msg = $i->issue->title;
                } else {
                    $msg = $i->message;
                }
            } else {
                $msg = $i->message;
            }

            $totalMin += $i->roundedMinutes();
            $totalMinRaw += $i->notRoundedMinutes();
            $totalCost += $i->price($i->roundedMinutes());
            $array[] = [
                'id' => $i->id,
                'date' => Yii::$app->formatter->asDate($i->created_at),
                'user' => $i->user->firstname . ' ' . $i->user->lastname,
                'composedMessage' => $msg,
                'costunitName' => $i->costunit ? $i->costunit->customer->name . ' / ' . $i->costunit->name : '-',
                'issueTitle' => $i->issue ? $i->issue->title : null,
                'issueLink' => $i->issue ? $i->issue->gitlab_url : null,
                'message' => $i->message,
                'comment' => $i->comment,
                'is_not_chargeable' => $i->is_not_chargeable,
                'is_charged' => Yii::$app->formatter->asBoolean($i->is_charged),
                'min' => $i->notRoundedMinutes(),
                'min_rounded' => $i->roundedMinutes(),
                'cost' => $i->formatPrice($i->price($i->roundedMinutes())),
                'cost_raw' => $i->price($i->roundedMinutes()),
            ];
        }

        return [
            'rows' => $array,
            'total_min' => $totalMin,
            'total_min_raw' => $totalMinRaw,
            'total_cost' => number_format($totalCost, 2, '.', '\''), //Yii::$app->formatter->asCurrency($totalCost, 'CHF'),
            'total_cost_raw' => $totalCost,
        ];
    }
}
