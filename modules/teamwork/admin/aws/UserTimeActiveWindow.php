<?php

namespace app\modules\teamwork\admin\aws;

use app\modules\teamwork\admin\traits\TimeDataGenerator;
use app\modules\teamwork\models\Time;
use luya\admin\ngrest\aw\CallbackButtonFileDownloadWidget;
use luya\admin\ngrest\base\ActiveWindow;
use luya\helpers\ExportHelper;

/**
 * User Time Active Window.
 *
 * File has been created with `aw/create` command.
 */
class UserTimeActiveWindow extends ActiveWindow
{
    use TimeDataGenerator;

    /**
     * @var string The name of the module where the ActiveWindow is located in order to finde the view path.
     */
    public $module = '@teamworkadmin';

    /**
     * Default label if not set in the ngrest model.
     *
     * @return string The name of of the ActiveWindow. This is displayed in the CRUD list.
     */
    public function defaultLabel()
    {
        return 'Time';
    }

    /**
     * Default icon if not set in the ngrest model.
     *
     * @var string The icon name from goolges material icon set (https://material.io/icons/)
     */
    public function defaultIcon()
    {
        return 'timer';
    }

    /**
     * The default action which is going to be requested when clicking the ActiveWindow.
     *
     * @return string The response string, render and displayed trough the angular ajax request.
     */
    public function index()
    {
        return $this->render('index', [
            'model' => $this->model,
            'data' => $this->generateArray($this->getData()),
        ]);
    }

    public function getData($from = null, $to = null)
    {
        return Time::find()
            ->with(['costunit', 'customer'])
            ->where(['user_id' => $this->model->id, 'is_charged' => false, 'is_not_chargeable' => false])
            ->all();
    }

    public function callbackExport()
    {
        $content = ExportHelper::csv($this->generateArray($this->getData())['rows']);
        return CallbackButtonFileDownloadWidget::sendOutput($this, 'Export.csv', $content);
    }
}
