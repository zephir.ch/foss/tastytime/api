<?php

namespace app\modules\teamwork\admin\aws;

use app\helpers\TimesheetPdf;
use app\modules\teamwork\admin\traits\TimeDataGenerator;
use app\modules\teamwork\models\Costunit;
use app\modules\teamwork\models\Time;
use luya\admin\ngrest\aw\CallbackButtonFileDownloadWidget;
use luya\admin\ngrest\base\ActiveWindow;
use yii\db\ActiveQuery;

/**
 * Export Active Window.
 *
 * File has been created with `aw/create` command.
 */
class ExportActiveWindow extends ActiveWindow
{
    use TimeDataGenerator;

    /**
     * @var string The name of the module where the ActiveWindow is located in order to finde the view path.
     */
    public $module = '@teamworkadmin';

    /**
     * Default label if not set in the ngrest model.
     *
     * @return string The name of of the ActiveWindow. This is displayed in the CRUD list.
     */
    public function defaultLabel()
    {
        return 'Exportieren';
    }

    public function getTitle()
    {
        return $this->model->name. ' Export';
    }

    /**
     * Default icon if not set in the ngrest model.
     *
     * @var string The icon name from goolges material icon set (https://material.io/icons/)
     */
    public function defaultIcon()
    {
        return 'explore';
    }

    /**
     * The default action which is going to be requested when clicking the ActiveWindow.
     *
     * @return string The response string, render and displayed trough the angular ajax request.
     */
    public function index()
    {
        return $this->render('index', [
            'model' => $this->model,
            'costUnits' => Costunit::find()->orderBy(['tw_customer.name' => SORT_ASC, 'tw_costunit.name' => SORT_ASC])->joinWith(['customer'])->all(),
        ]);
    }

    /**
     * Undocumented function
     *
     * @param [type] $from
     * @param [type] $to
     * @return ActiveQuery
     */
    public function generateQuery($from, $to)
    {
        $from = strtotime("midnight", (int) $from);
        $to = strtotime("+1 day midnight", (int) $to);

        return Time::find()
        ->joinWith(['user', 'issue'])
        ->where([
            'or',
            ['in', 'issue_id', $this->model->getIssues()->select(['tw_issue.id'])],
            ['=', 'costunit_id', $this->model->id]
        ])
        ->andWhere(
            ['between', 'tw_time.created_at', $from, $to]
        );
    }

    public function callbackList($from, $to)
    {
        $q = $this->generateQuery($from, $to)->all();

        return $this->generateArray($q);
    }

    public function callbackUpdate($id, $message)
    {
        $item = Time::findOne($id);

        if ($item) {
            $item->message = $message;

            if ($item->save(true, ['message'])) {
                return $this->sendSuccess("Updated success.");
            }
        }

        return $this->sendError("Error update");
    }

    public function callbackUpdateComment($id, $comment)
    {
        $item = Time::findOne($id);

        if ($item) {
            $item->comment = $comment;

            if ($item->save(true, ['comment'])) {
                return $this->sendSuccess("Updated success.");
            }
        }

        return $this->sendError("Error update");
    }

    public function callbackNotChargeable($id, $value)
    {
        $item = Time::findOne($id);

        if ($item) {
            $item->is_not_chargeable = (int) $value;

            if ($item->save(true, ['is_not_chargeable'])) {
                return $this->sendSuccess("Updated success.");
            }
        }

        return $this->sendError("Error update");
    }

    public function callbackDuration($id, $minutes)
    {
        $item = Time::findOne($id);

        if ($item) {
            $item->duration = $minutes * 60;

            if ($item->save(true, ['duration'])) {
                return $this->sendSuccess("Updated success.");
            }
        }

        return $this->sendError("Error update");
    }

    public function callbackMove($id, $newCostUnitId)
    {
        $item = Time::findOne($id);

        if (!$item) {
            return $this->sendError("Unable to find item.");
        }

        $item->costunit_id = $newCostUnitId;
        if ($item->save()) {
            if ($item->issue) {
                // move all time entries for the given issue, to have a consistent behavior, otherwise
                // wen setting project_id 0 the time entires could not be found anymore.
                foreach (Time::find()->andWhere(['issue_id' => $item->issue->id])->all() as $otherTimeEntires) {
                    $otherTimeEntires->costunit_id = $newCostUnitId;
                    $otherTimeEntires->save(true, ['costunit_id']);
                }

                $issue = $item->issue;
                $issue->project_id = 0;
                if (!$issue->save(true, ['project_id'])) {
                    return $this->sendError('Error', $item->issue->getErrors());
                }
            }

            return $this->sendSuccess("Moved.");
        }

        return $this->sendError('Error', $item->getErrors());
    }

    // generators

    public function callbackPdf($from, $to, $markCharged)
    {
        $from = (int) $from;
        $to = (int) $to;
        $q = $this->generateQuery($from, $to)->andWhere(['is_not_chargeable' => 0])->orderby('created_at ASC');
        $array = $this->generateArray($q->all());

        if ($markCharged) {
            Time::updateAll(['is_charged' => 1], ['in', 'id', $q->column()]);
        }

        $costunitName = $this->model->name;
        $customerName = $this->model->customer->name;

        $pdf = TimeSheetPdf::create($array, $from, $to, $customerName, $costunitName);

        $content = $pdf->Output('temp.pdf', 'S');

        return CallbackButtonFileDownloadWidget::sendOutput($this, 'Zeiterfassung ' . $customerName . ' – ' . $costunitName . '.pdf', $content);
    }
}
