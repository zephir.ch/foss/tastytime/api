<?php

namespace app\modules\teamwork\admin\apis;

use yii\db\ActiveQuery;

/**
 * Customer Controller.
 *
 * File has been created with `crud/create` command.
 */
class CustomerController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\teamwork\models\Customer';

    public function prepareIndexQuery()
    {
        $this->pagination = false;

        return parent::prepareIndexQuery()
            ->joinWith(['costunits' => function (ActiveQuery $query) {
                $query->orderBy(['name' => SORT_ASC])->andWhere(['tw_costunit.is_active' => true]);
            }])
            ->joinWith(['userFavorite'], true, 'LEFT JOIN')
            ->orderBy(['sortindex' => SORT_DESC, 'name' => SORT_ASC]);
    }
}
