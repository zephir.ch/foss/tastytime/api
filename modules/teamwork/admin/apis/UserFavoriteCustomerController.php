<?php

namespace app\modules\teamwork\admin\apis;

/**
 * User Favorite Customer Controller.
 *
 * File has been created with `crud/create` command.
 */
class UserFavoriteCustomerController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\teamwork\models\UserFavoriteCustomer';
}
