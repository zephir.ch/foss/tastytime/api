<?php

namespace app\modules\teamwork\admin\apis;

use app\modules\teamwork\models\Costunit;
use luya\admin\components\Auth;
use Yii;

/**
 * Costunit Controller.
 *
 * File has been created with `crud/create` command.
 */
class CostunitController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\teamwork\models\Costunit';

    public function prepareListQuery()
    {
        return parent::prepareListQuery()->joinWith(['customer']);
    }

    public function actionPermissions()
    {
        return [
            'stats' => Auth::CAN_VIEW,
        ];
    }

    public function actionStats($id)
    {
        $costunit = Costunit::findOne(['id' => $id]);

        $minutesBudget = $costunit->time_budget_hours * 60;

        $total = (int) ceil($costunit->getTotalTimeAll()->sum('duration') / 60);

        $myTime = (int) ceil($costunit->getTotalTimeAll()->andWhere(['user_id' => Yii::$app->jwt->identity->id])->sum('duration') / 60);

        return [
            'budget' => empty($minutesBudget) ? 0 : round(($total / $minutesBudget) * 100, 1),
            'total_time' => ceil($total / 60),
            'my_total_time' => ceil($myTime / 60),
        ];
    }
}
