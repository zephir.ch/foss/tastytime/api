<?php

namespace app\modules\teamwork\admin\apis;

use app\modules\teamwork\models\GitlabProject;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * Gitlab Project Controller.
 *
 * File has been created with `crud/create` command.
 */
class GitlabProjectController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\teamwork\models\GitlabProject';

    public $truncateAction = false;

    public function actionRequireCostunit()
    {
        return new ActiveDataProvider([
            'query' => GitlabProject::find()->where(['is', 'costunit_id', new Expression('null')]),
        ]);
    }
}
