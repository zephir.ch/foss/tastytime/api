<?php

namespace app\modules\teamwork\admin\apis;

use app\modules\teamwork\models\User;
use app\modules\teamwork\models\UserFavoriteCustomer;
use luya\admin\base\JwtIdentityInterface;
use luya\admin\components\Auth;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * User Controller.
 *
 * File has been created with `crud/create` command.
 */
class UserController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\teamwork\models\User';

    public $authOptional = ['login'];

    public function actionPermissions()
    {
        return [
            'session' => Auth::CAN_VIEW,
        ];
    }

    public function actionLogin()
    {
        $email = Yii::$app->request->getBodyParam('email');
        $password = Yii::$app->request->getBodyParam('password');

        /** @var JwtIdentityInterface|Model $user */
        $user = User::find()->where(['email' => $email])->one();

        if ($user && Yii::$app->security->validatePassword($password, $user->password)) {
            $user->jwt = Yii::$app->jwt->generateToken($user);
            if ($user->save()) {
                return $user;
            }

            return $this->sendModelError($user);
        }

        return $this->sendArrayError(['email' => 'User not found or password is wrong.']);
    }

    public function actionFavoriteCustomers()
    {
        return new ActiveDataProvider([
            'query' => UserFavoriteCustomer::find()->andWhere(['user_id' => Yii::$app->jwt->identity->id]),
            'pagination' => false,
        ]);
    }

    public function actionSession()
    {
        return Yii::$app->jwt->identity;
    }
}
