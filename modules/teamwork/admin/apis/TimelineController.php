<?php

namespace app\modules\teamwork\admin\apis;

use app\modules\teamwork\models\Time;
use luya\admin\base\RestController;
use luya\admin\ngrest\base\actions\OptionsAction;
use luya\helpers\ArrayHelper;
use Yii;

class TimelineController extends RestController
{
    public $authOptional = ['options'];

    public function permissionRoute(\yii\base\Action $action)
    {
        return 'jwtuser/generic';
    }

    public function actions()
    {
        return [
            'options' => OptionsAction::class,
        ];
    }

    public function actionIndex($date)
    {
        $timestamp = strtotime($date);

        $start = strtotime('today', $timestamp);
        $end = strtotime('tomorrow', $timestamp) - 1;

        $times = Time::find()
            ->where([
                'and',
                ['=', 'tw_time.user_id', Yii::$app->jwt->identity->id],
                ['between', 'tw_time.created_at', $start, $end]
            ])
            ->joinWith(['issue.gitlabProject.costunit', 'costunit', 'customer'])
            ->orderby(['is_active' => SORT_DESC, 'updated_at' => SORT_DESC])
            ->asArray()
            ->all();

        return ArrayHelper::typeCast($times);
    }
}
