<?php

namespace app\modules\teamwork\admin\apis;

use Yii;

/**
 * Issue Controller.
 *
 * File has been created with `crud/create` command.
 */
class IssueController extends \luya\admin\ngrest\base\Api
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\teamwork\models\Issue';

    public $pagination = ['defaultPageSize' => 100];

    public $truncateAction = false;

    public function prepareIndexQuery()
    {
        Yii::$app->jwt->identity->fetchGitLabIssues();
        return parent::prepareIndexQuery()->andWhere(['user_id' => Yii::$app->jwt->identity->id, 'is_closed' => false]);
    }
}
