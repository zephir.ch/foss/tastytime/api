<?php

use yii\db\Migration;

/**
 * Class m191117_091009_basetables
 */
class m191125_091009_token_text extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('tw_user', 'jwt');
        $this->addColumn('tw_user', 'jwt', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('tw_user', 'jwt', $this->string());
    }
}
