<?php

use yii\db\Migration;

/**
 * Class m200114_175427_is_charged
 */
class m200114_175427_is_charged extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('tw_time', 'is_exported', 'is_charged');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('tw_time', 'is_charged', 'is_exported');
    }
}
