<?php

use yii\db\Migration;

/**
 * Class m191117_091009_basetables
 */
class m191126_091009_is_not_chargeable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tw_time', 'is_not_chargeable', $this->boolean()->defaultValue(false));
        $this->addColumn('tw_time', 'exported_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tw_time', 'is_not_chargeable');
        $this->dropColumn('tw_time', 'exported_at');
    }
}
