<?php

use yii\db\Migration;

/**
 * Class m200114_175427_is_charged
 */
class m210610_165327_costunitbudge extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tw_costunit', 'time_budget_hours', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tw_costunit', 'time_budget_hours');
    }
}
