<?php

use yii\db\Migration;

/**
 * Class m191117_091009_basetables
 */
class m191117_091009_basetables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tw_user', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),
            'password' => $this->string()->notNull()->unique(),
            'jwt' => $this->string()->unique(),
            'firstname' => $this->string()->notNull(),
            'lastname' => $this->string()->notNull(),
            'gitlab_token' => $this->string(),
            'gitlab_user_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable('tw_customer', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'color' => $this->string(),
            'is_active' => $this->boolean()->defaultValue(true),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable('tw_costunit', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'customer_id' => $this->integer()->notNull(),
            'is_active' => $this->boolean()->defaultValue(true),
            'is_message_required' => $this->boolean()->defaultValue(true),
            'is_billable' => $this->boolean()->defaultValue(false),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('customer_id', 'tw_costunit', 'customer_id');

        $this->createTable('tw_issue', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'project' => $this->string(),
            'project_id' => $this->integer(),
            'milestone' => $this->string(),
            'labels' => $this->text(),
            'gitlab_id' => $this->integer(),
            'gitlab_url' => $this->string(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('user_id', 'tw_issue', 'user_id');
        $this->createIndex('gitlab_id', 'tw_issue', 'gitlab_id');

        $this->createTable('tw_gitlab_project', [
            'id' => $this->primaryKey(),
            'costunit_id' => $this->integer(),
            'gitlab_id' => $this->integer(),
            'title' => $this->string(),
        ]);

        $this->createTable('tw_time', [
            'id' => $this->primaryKey(),
            'user_id' => $this->string()->notNull(),
            'issue_id' => $this->integer(),
            'costunit_id' => $this->integer(),
            'message' => $this->text(),
            'duration' => $this->integer()->defaultValue(0),
            'is_active' => $this->boolean()->defaultValue(false),
            'is_interruption' => $this->boolean()->defaultValue(false),
            'is_exported' => $this->boolean()->defaultValue(false),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('user_id', 'tw_time', 'user_id');
        $this->createIndex('issue_id', 'tw_time', 'issue_id');
        $this->createIndex('costunit_id', 'tw_time', 'costunit_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tw_user');

        $this->dropTable('tw_customer');

        $this->dropTable('tw_costunit');

        $this->dropTable('tw_issue');

        $this->dropTable('tw_gitlab_project');

        $this->dropTable('tw_time');
    }
}
