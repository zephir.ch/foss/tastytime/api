<?php

use yii\db\Migration;

/**
 * Class m191204_145140_teamworkadmin
 */
class m191204_145140_teamworkadmin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tw_issue', 'is_closed', $this->boolean()->defaultValue(false));
        $this->addColumn('tw_time', 'comment', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tw_issue', 'is_closed');
        $this->dropColumn('tw_time', 'comment');
    }
}
