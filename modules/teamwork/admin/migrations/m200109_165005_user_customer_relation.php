<?php

use yii\db\Migration;

/**
 * Class m200109_165005_user_customer_relation
 */
class m200109_165005_user_customer_relation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tw_user_favorite_customer', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'customer_id' => $this->integer()->notNull(),
            'sortindex' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('user_id', 'tw_user_favorite_customer', 'user_id');
        $this->createIndex('customer_id', 'tw_user_favorite_customer', 'customer_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tw_user_favorite_customer');
    }
}
