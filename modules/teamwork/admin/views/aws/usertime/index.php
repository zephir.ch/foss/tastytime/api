<?php

/**
 * UserTimeActiveWindow Index View.
 *
 * @var \luya\admin\ngrest\base\ActiveWindowView $this
 * @var \luya\admin\ngrest\base\NgRestModel $model
 */

use luya\admin\ngrest\aw\CallbackButtonFileDownloadWidget;

/** @var array $data */
?>
<p>Einträge für <?= $model->email; ?> welche nicht verrechnet wurden und verrechenbar sind.</p>
<table class="table table-border table-hover table-striped">
<thead>
    <tr>
        <th>Datum</th>
        <th>Kostenstelle</th>
        <th>Text</th>
        <th>Minuten (Gerundet)</th>
        <th>Kosten</th>
    </tr>
</thead>
<?php foreach ($data['rows'] as $time): ?>
<tr>
    <td>
        <?= $time['date']; ?>
    </td>
    <td>
        <?= $time['costunitName']; ?>
    </td>
    <td>
        <?= $time['composedMessage']; ?></td>
    </td>

    <td>
        <?= $time['min_rounded']; ?>
    </td>
    <td>
        <?= $time['cost']; ?>
    </td>
</tr>
<?php endforeach; ?>
<tr>
    <td colspan="3">

    </td>
    <td>
        <b><?= $data['total_min']; ?></b>
    </td>
    <td>
        <b><?= $data['total_cost']; ?></b>
    </td>
</tr>
</table>
<?= CallbackButtonFileDownloadWidget::widget(['callback' => 'export', 'label' => 'Export']); ?>
