<?php

/**
 * ExportActiveWindow Index View.
 *
 * @var $this \luya\admin\ngrest\base\ActiveWindowView
 * @var $model \luya\admin\ngrest\base\NgRestModel
 */

use luya\admin\helpers\Angular;

/** @var array $costUnits */
?>
<script>
zaa.bootstrap.register('ExportController', ['$scope', function($scope) {

    $scope.dateFrom = (new Date().getTime() / 1000) - (30 * 24 * 60 * 60);
    $scope.dateTo = new Date().getTime() / 1000;
    $scope.includedExportet = 0;
    $scope.markCharged = 0;

    $scope.data = [];

    $scope.load = function() {
        $scope.$parent.sendActiveWindowCallback('list', {from:$scope.dateFrom,to:$scope.dateTo}).then(function(response) {
            $scope.data = response.data;
        });
    };

    $scope.downloadUrl;

    $scope.pdf = function() {
        $scope.$parent.sendActiveWindowCallback('pdf', {from:$scope.dateFrom,to:$scope.dateTo, markCharged:$scope.markCharged}).then(function(response) {
            $scope.downloadUrl = response.data.responseData.url;
        });
    };

    $scope.updateText = function(item) {
        $scope.$parent.sendActiveWindowCallback('update', {id: item.id, message: item.message}).then(function(response) {
            $scope.$parent.toast.success('Text update has been saved.');
        });
    };

    $scope.updateComment = function(item) {
        $scope.$parent.sendActiveWindowCallback('update-comment', {id: item.id, comment: item.comment}).then(function(response) {
            $scope.$parent.toast.success('Comment update has been saved.');
        });
    };

    $scope.updateNotChargeable = function(item) {
        $scope.$parent.sendActiveWindowCallback('not-chargeable', {id: item.id, value: item.is_not_chargeable}).then(function(response) {
            $scope.$parent.toast.success('has been saved.');
        });
    };

    $scope.updateDuration = function(item) {
        $scope.$parent.sendActiveWindowCallback('duration', {id: item.id, minutes: item.min}).then(function(response) {
            $scope.$parent.toast.success('has been saved.');
        });
    };

    $scope.setDateAndLoad = function(from, to){
        $scope.dateFrom = from;
        $scope.dateTo = to;
        $scope.load();
    };

    $scope.moveItem = function(item, newCostUnitId) {
        $scope.$parent.sendActiveWindowCallback('move', {id: item.id, newCostUnitId: newCostUnitId}).then(function(response) {
            $scope.load();
        });
    };

}]);
</script>

<div ng-controller="ExportController">
<ul class="list-group list-group-horizontal mb-3">
  <li class="list-group-item"><button type="button" class="btn" ng-click="setDateAndLoad(<?= strtotime("first day of this month"); ?>, <?= strtotime("last day of this month"); ?>)">This month</li>
  <li class="list-group-item"><button type="button" class="btn" ng-click="setDateAndLoad(<?= strtotime("first day of last month"); ?>, <?= strtotime("last day of last month"); ?>)">Last month</li>
  <li class="list-group-item"><button type="button" class="btn" ng-click="setDateAndLoad(<?= strtotime("first day of last month"); ?>, <?= strtotime("last day of this month"); ?>)">This &amp; last month</li>
</ul>
    <?= Angular::date('dateFrom', 'From'); ?>
    <?= Angular::date('dateTo', 'To'); ?>
    <button type="button" class="btn btn-info" ng-click="load()">Load Data</button>
    <hr />
    <div ng-show="data.rows.length == 0" class="alert alert-warning mt-3">No data available</div>
    <div ng-show="data.rows.length > 0">
        <table class="table table-striped table-bordered table-hover mt-3">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Person</th>
                    <th>Comment</th>
                    <th>Text</th>
                    <th>Min.</th>
                    <th>Gerundet</th>
                    <th>Kosten</th>
                    <th>Nicht verrechenbar</th>
                    <th>Verrechnet</th>
                    <th></th>
                </tr>
            </thead>
            <tr ng-repeat="i in data.rows">
                <td width="50"><small>{{ i.date }}</small></td>
                <td width="100"><small>{{ i.user }}</small></td>
                <td>
                    <textarea ng-model="i.comment" ng-model-options="{ debounce: 500 }" class="form-control" ng-change="updateComment(i)"></textarea>
                </td>
                <td>
                    <textarea ng-model="i.message" ng-model-options="{ debounce: 500 }" class="form-control" ng-change="updateText(i)"></textarea>
                    <span ng-show="i.issueTitle" class="text-muted mt-1">
                        Issue: <a ng-if="i.issueTitle" target="_blank" ng-href="{{i.issueLink}}">{{ i.issueTitle }}</a>
                    </span>
                </td>
                <td width="60"><input type="text" style="width:60px;" ng-model="i.min" ng-model-options="{debounce:500}" class="form-control" ng-change="updateDuration(i)" /></td>
                <td width="120">{{ i.min_rounded }} min</td>
                <td width="60">{{ i.cost }}</td>
                <td>
                    <label>
                        <input 
                            style="display:inline-block"
                            type="checkbox"
                            ng-model="i.is_not_chargeable"
                            ng-change="updateNotChargeable(i)"
                            ng-true-value="1"
                            ng-false-value="0"
                        >
                    </label
                ></td>
                <td width="60">{{i.is_charged}}</td>
                <td>
                    <button type="button" class="btn" ng-click="showMove=1">Move</button>
                    <div ng-show="showMove">
                        <select class="form-control my-2" ng-model="newCostUnitId">
                            <option value="">-</option>
                            <?php foreach ($costUnits as $cu): ?>
                            <option value="<?= $cu->id; ?>"><?= $cu->customer->name; ?> / <?= $cu->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <button ng-show="showMove" class="btn" type="button" ng-click="showMove=0">Abort</button>
                        <button ng-show="newCostUnitId" class="btn btn-success" type="button" ng-click="moveItem(i, newCostUnitId)">Save</button>
                    </div>
                </td>
            </tr>
            <tr>
                <th colspan="7">Total</th>
                <th>{{ data.total_min }} min</th>
                <th>{{ data.total_cost }}</th>
                <th></th>
                <th></th>
            </tr>
        </table>

        <label style="display:inline-block; margin:0px; padding:0px;" class="my-3">
            <input style="display:inline-block; font-weight:bold" type="checkbox" ng-model="markCharged" /> und Positionen als <b>Verrechnet</b> markieren
        </label>
        <button type="button" ng-click="pdf()" class="btn btn-primary">Generate PDF</button>
        <a ng-show="downloadUrl" ng-href="{{downloadUrl}}" target="_blank" class="btn btn-success">Download</a>
    </div>
</div>