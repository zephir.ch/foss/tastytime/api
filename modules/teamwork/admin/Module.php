<?php

namespace app\modules\teamwork\admin;

use app\modules\teamwork\models\UserFavoriteCustomer;
use luya\admin\dashboard\ListDashboardObject;
use luya\admin\models\Config;

/**
 * Teamwork Admin Module.
 *
 * File has been created with `module/create` command.
 *
 * @property int $hourRate
 *
 * @author
 * @since 1.0.0
 */
class Module extends \luya\admin\base\Module
{
    public $readyLabel = 'status: Ready';

    private $_hourRate;

    public function getHourRate()
    {
        if ($this->_hourRate === null) {
            $this->_hourRate = Config::get('tastytime_hour_rate', 160);
        }

        return $this->_hourRate;
    }

    public function setHourRate($rate)
    {
        $this->_hourRate = $rate;
    }

    public $dashboardObjects = [
        [
            'class' => ListDashboardObject::class,
            'dataApiUrl' => 'admin/gitlab-projects/require-costunit',
            'title' => 'Projects without Costunit',
            'template' => '<li class="list-group-item" ng-repeat="item in data">{{item.title}}</li>'
        ]
    ];

    public $apis = [
        'users' => 'app\modules\teamwork\admin\apis\UserController',
        'issues' => 'app\modules\teamwork\admin\apis\IssueController',
        'customers' => 'app\modules\teamwork\admin\apis\CustomerController',
        'costunits' => 'app\modules\teamwork\admin\apis\CostunitController',
        'gitlab-projects' => 'app\modules\teamwork\admin\apis\GitlabProjectController',
        'time' => 'app\modules\teamwork\admin\apis\TimeController',
        'timeline' => 'app\modules\teamwork\admin\apis\TimelineController',
        'user-favorite-customer' => 'app\modules\teamwork\admin\apis\UserFavoriteCustomerController',

    ];

    public $apiRules = [
        'users' => [
            'extraPatterns' => [
                'OPTIONS session' => 'options',
            ]
        ],
        'timeline' => [
            'extraPatterns' => [
                'OPTIONS index' => 'options',
            ]
        ],
        'costunits' => [
            'extraPatterns' => [
                'GET {id}/stats' => 'stats',
                'OPTIONS {id}/stats' => 'stats',
            ]
        ],
    ];

    public function getMenu()
    {
        return (new \luya\admin\components\AdminMenuBuilder($this))
            ->node('Timework', 'money')
                ->group('Benutzer')
                    ->itemApi('Benutzer', 'teamworkadmin/user/index', 'groups', 'users')
                    ->itemApi('Benutzer Favoriten', 'teamworkadmin/user-favorite-customer/index', 'label', UserFavoriteCustomer::ngRestApiEndpoint())
                ->group('Manager')
                    ->itemApi('Kostenstelle', 'teamworkadmin/costunit/index', 'star', 'costunits')
                    ->itemApi('Kunden', 'teamworkadmin/customer/index', 'groups', 'customers')
                    ->itemApi('Gitlab Projekte', 'teamworkadmin/gitlab-project/index', 'label', 'gitlab-projects')
                ->group('Daten')
                    ->itemApi('Issue', 'teamworkadmin/issue/index', 'label', 'issues')
                    ->itemApi('Zeit', 'teamworkadmin/time/index', 'timer', 'time');
    }

    public function extendPermissionRoutes()
    {
        return [
            ['route' => 'jwtuser/generic', 'alias' => 'Generic JWT User Permission'],
        ];
    }
}
