<?php

namespace app\modules\teamwork\admin\buttons;

use app\modules\teamwork\models\Costunit;
use luya\admin\ngrest\base\ActiveButton;
use luya\admin\ngrest\base\NgRestModel;

class MarkAsUnchargeable extends ActiveButton
{
    public function getDefaultIcon()
    {
        return 'cleaning_services';
    }

    public function getDefaultLabel()
    {
        return 'Mark all as Not Chargeable';
    }

    public function handle(NgRestModel $model)
    {
        /** @var Costunit $model */
        if (!$model->is_billable) {
            $count = 0;
            foreach ($model->getTotalTime()->all() as $time) {

                if ($time->is_not_chargeable) {
                    // skip, this its already marked
                } else {
                    $time->is_not_chargeable = 1;
                    if ($time->save(false, ['is_not_chargeable'])) {
                        $count++;
                    }
                }
            }

            $this->sendReloadEvent();

            return $this->sendSuccess("{$count} items marked as not chargeable.");
        }

        return $this->sendError("Only not billable cost units can be processed.");
    }
}
