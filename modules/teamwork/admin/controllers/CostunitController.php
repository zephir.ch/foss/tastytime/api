<?php

namespace app\modules\teamwork\admin\controllers;

/**
 * Costunit Controller.
 *
 * File has been created with `crud/create` command.
 */
class CostunitController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\teamwork\models\Costunit';
}
