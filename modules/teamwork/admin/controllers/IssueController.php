<?php

namespace app\modules\teamwork\admin\controllers;

/**
 * Issue Controller.
 *
 * File has been created with `crud/create` command.
 */
class IssueController extends \luya\admin\ngrest\base\Controller
{
    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\teamwork\models\Issue';

    public $clearButton = false;
}
