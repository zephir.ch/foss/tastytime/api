# Tasty Time API

The php LUYA api application image running on **port 80**.

## ENV VARS

|var|required|description
|---|--------|-----------
|`DB_DSN`|yes|Database DSN `mysql:host=HOST;dbname=DB_NAME`
|`DB_USERNAME`|yes|Datbase User name
|`DB_PASSWORD`|yes|Database Password
|`APP_TITLE`|yes|Company Teamwork
|`APP_JWT_SECRET`|yes|A random secret for jwt hashing
|`APP_JWT_EMAIL`|yes|The e-mail adresses which matches the LUYA Api User.
|`APP_DEBUG`|no|Whether application should print debug informations or not, should be disabled in production as it could leak database 
informations.
|`APP_READY_LABEL`|no|The label of the issues, if not defined default value is `status: Ready`.

## LUYA CONFIG

Use the LUYA admin config to define those variables:

+ tastytime_pdf_author
+ tastytime_hour_rate (default is 160)
+ tastytime_flat_fee (percentage of fee to add to the summary, if 0 this is disable)