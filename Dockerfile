FROM registry.gitlab.com/zephir.ch/foss/luya-docker:php82

## Replace the default server name `luya` with your own server name
RUN sed -i 's/server_name luya;/server_name tastytimeapi;/g' /etc/nginx/conf.d/default.conf

COPY . /var/www/html
COPY php.ini /etc/php82/conf.d/zzz_custom.ini

RUN mkdir -p /var/www/html/public_html/assets
RUN mkdir -p /var/www/html/runtime

RUN chmod 777 /var/www/html/public_html/assets
RUN chmod 777 /var/www/html/runtime
