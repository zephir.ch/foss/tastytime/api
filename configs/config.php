<?php

use luya\Config;

$config = new Config('tastytime', dirname(__DIR__), [
    'siteTitle' => getenv('APP_TITLE'),
    'timeZone' => 'Europe/Zurich',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
        'admin' => [
            'class' => 'luya\admin\Module',
            'cors' => true,
            'jsonCruft' => false,
            'secureLogin' => false,
            'strongPasswordPolicy' => false,
            'interfaceLanguage' => 'de',
        ],
        'teamworkadmin' => [
            'class' => 'app\modules\teamwork\admin\Module',
            'readyLabel' => !empty(getenv('APP_READY_LABEL')) ? getenv('APP_READY_LABEL') : 'status: Ready',
        ],
    ],
    'components' => [
        'jwt' => [
            'class' => 'luya\admin\components\Jwt',
            'key' => getenv('APP_JWT_SECRET'),
            'apiUserEmail' => getenv('APP_JWT_EMAIL'),
            'identityClass' => 'app\modules\teamwork\models\User',
            'expireTime' => (60 * 60 * 24 * 90), // 3 months
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8mb4',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
            'sessionTable' => 'admin_session',
        ],
        'composition' => [
            'hidden' => true,
            'default' => ['langShortCode' => 'de'],
        ],
        'view' => [
            'class' => 'luya\web\View',
            'autoRegisterCsrf' => false,
        ],
    ]
]);

if (getenv('APP_DEBUG')) {
    $config->callback(function () {
        define('YII_DEBUG', true);
        define('YII_ENV', 'local');
    });
}


$config->webComponent('request', [
    'secureHeaders' => [],
]);

/********** LOCAL ************** */

$config->env(Config::ENV_LOCAL, function (Config $config) {

    // debug and gii on local env
    $config->module('debug', [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ]);
    $config->module('gii', [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['*'],
    ]);

    $config->component('cache', [
        'class' => 'yii\caching\DummyCache',
    ]);

    $config->bootstrap(['debug', 'gii']);
});



/********** PROD ************* */

$config->env(Config::ENV_PROD, function (Config $config) {
    $config->component('db', [
        'enableSchemaCache' => true,
        'schemaCacheDuration' => 0,
    ]);

    $config->component('assetManager', [
        'class' => 'luya\web\AssetManager',
        'hashCallback' => function ($path) {
            return hash('md4', $path);
        },
        'forceCopy' => false,
        'appendTimestamp' => true,
    ]);
});



return $config;
